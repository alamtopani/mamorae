# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160519093739) do

  create_table "blogs", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "user_id"
    t.string   "category"
    t.date     "start_at"
    t.date     "end_at"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "buzzer_industries", force: :cascade do |t|
    t.integer  "buzzer_profile_id"
    t.integer  "industry_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "buzzer_jobs", force: :cascade do |t|
    t.integer  "buzzer_id"
    t.integer  "job_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "facebook_post_id"
  end

  create_table "buzzer_profiles", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.datetime "born"
    t.string   "gender"
    t.string   "country"
    t.string   "province_state"
    t.string   "city"
    t.string   "phone"
    t.integer  "mamoney"
    t.string   "bank_name"
    t.string   "branch"
    t.string   "account_number"
    t.integer  "buzzer_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "facebook_access_token"
    t.string   "facebook_expires_in"
    t.string   "facebook_signed_request"
    t.string   "facebook_user_id"
  end

  add_index "buzzer_profiles", ["buzzer_id"], name: "index_buzzer_profiles_on_buzzer_id"

  create_table "buzzer_vouchers", force: :cascade do |t|
    t.string   "status",          default: "pending"
    t.integer  "transfer_money"
    t.integer  "paid_money"
    t.integer  "buzzer_id"
    t.integer  "voucher_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "voucher_code_id"
  end

  add_index "buzzer_vouchers", ["voucher_code_id"], name: "index_buzzer_vouchers_on_voucher_code_id"

  create_table "images", force: :cascade do |t|
    t.string   "type"
    t.string   "status",              default: "waiting"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "photo"
    t.integer  "buzzer_profile_id"
    t.integer  "merchant_profile_id"
    t.integer  "job_id"
    t.integer  "voucher_id"
  end

  add_index "images", ["buzzer_profile_id"], name: "index_images_on_buzzer_profile_id"
  add_index "images", ["job_id"], name: "index_images_on_job_id"
  add_index "images", ["merchant_profile_id"], name: "index_images_on_merchant_profile_id"
  add_index "images", ["voucher_id"], name: "index_images_on_voucher_id"

  create_table "industries", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "jobs", force: :cascade do |t|
    t.string   "title"
    t.string   "description"
    t.date     "start_date"
    t.date     "end_date"
    t.string   "url"
    t.string   "facebook_mention"
    t.string   "facebook_hashtag"
    t.integer  "merchant_id"
    t.integer  "package_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "jobs", ["merchant_id"], name: "index_jobs_on_merchant_id"
  add_index "jobs", ["package_id"], name: "index_jobs_on_package_id"

  create_table "merchant_industries", force: :cascade do |t|
    t.integer  "merchant_profile_id"
    t.integer  "industry_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "merchant_profiles", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "website"
    t.string   "admin_name"
    t.string   "phone_admin"
    t.string   "address_1"
    t.string   "address_2"
    t.string   "country"
    t.string   "province_state"
    t.string   "city"
    t.string   "postal"
    t.string   "bio"
    t.string   "bank_name"
    t.string   "branch"
    t.string   "account_number"
    t.integer  "merchant_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "merchant_profiles", ["merchant_id"], name: "index_merchant_profiles_on_merchant_id"

  create_table "packages", force: :cascade do |t|
    t.string   "category"
    t.integer  "buzzer_hired"
    t.integer  "campaign_placement_month"
    t.integer  "campaign_placement_minute"
    t.integer  "price"
    t.string   "status"
    t.integer  "merchant_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "packages", ["merchant_id"], name: "index_packages_on_merchant_id"

  create_table "problems", force: :cascade do |t|
    t.string   "subject"
    t.string   "email"
    t.string   "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "status"
  end

  create_table "voucher_codes", force: :cascade do |t|
    t.string   "code"
    t.boolean  "has_redeemed"
    t.date     "when_redeemed"
    t.integer  "buzzer_id"
    t.integer  "voucher_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "voucher_codes", ["buzzer_id"], name: "index_voucher_codes_on_buzzer_id"
  add_index "voucher_codes", ["voucher_id"], name: "index_voucher_codes_on_voucher_id"

  create_table "vouchers", force: :cascade do |t|
    t.string   "title"
    t.string   "description"
    t.date     "start_date"
    t.date     "end_date"
    t.integer  "amount"
    t.string   "kind"
    t.string   "condition"
    t.integer  "merchant_id"
    t.integer  "package_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "normal_price"
    t.integer  "discounted_price"
  end

  add_index "vouchers", ["merchant_id"], name: "index_vouchers_on_merchant_id"
  add_index "vouchers", ["package_id"], name: "index_vouchers_on_package_id"

end
