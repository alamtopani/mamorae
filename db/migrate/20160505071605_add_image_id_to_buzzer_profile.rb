class AddImageIdToBuzzerProfile < ActiveRecord::Migration
  def change
    add_reference :images, :buzzer_profile, index: true
  end
end
