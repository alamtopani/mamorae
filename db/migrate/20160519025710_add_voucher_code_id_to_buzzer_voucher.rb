class AddVoucherCodeIdToBuzzerVoucher < ActiveRecord::Migration
  def change
    add_reference :buzzer_vouchers, :voucher_code, index: true
  end
end
