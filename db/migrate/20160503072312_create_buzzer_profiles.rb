class CreateBuzzerProfiles < ActiveRecord::Migration
  def change
    create_table :buzzer_profiles do |t|
      t.string :first_name
      t.string :last_name
      t.datetime :born
      t.string :gender
      t.string :country
      t.string :province_state
      t.string :city
      t.string :phone
      t.integer :mamoney
      t.string :bank_name
      t.string :branch
      t.string :account_number
      t.references :buzzer, index: true

      t.timestamps null: false
    end
  end
end
