class AddFacebookToBuzzerProfile < ActiveRecord::Migration
  def change
    add_column :buzzer_profiles, :facebook_access_token, :string
    add_column :buzzer_profiles, :facebook_expires_in, :string
    add_column :buzzer_profiles, :facebook_signed_request, :string
    add_column :buzzer_profiles, :facebook_user_id, :string
  end
end
