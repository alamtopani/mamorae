class CreateBuzzerVouchers < ActiveRecord::Migration
  def change
    create_table :buzzer_vouchers do |t|
      t.string :status, default: "pending"
      t.integer :transfer_money
      t.integer :paid_money
      t.belongs_to :buzzer
      t.belongs_to :voucher

      t.timestamps null: false
    end
  end
end
