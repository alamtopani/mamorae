class AddPriceToVoucher < ActiveRecord::Migration
  def change
    add_column :vouchers, :normal_price, :integer
    add_column :vouchers, :discounted_price, :integer
  end
end
