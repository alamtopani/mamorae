class AddJobIdToImage < ActiveRecord::Migration
  def change
    add_reference :images, :job, index: true
  end
end
