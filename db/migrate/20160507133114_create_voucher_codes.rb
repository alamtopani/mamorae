class CreateVoucherCodes < ActiveRecord::Migration
  def change
    create_table :voucher_codes do |t|
      t.string :code
      t.boolean :has_redeemed
      t.date :when_redeemed
      t.references :buzzer, index: true
      t.references :voucher, index: true

      t.timestamps null: false
    end
  end
end
