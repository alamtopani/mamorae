class CreateMerchantIndustries < ActiveRecord::Migration
  def change
    create_table :merchant_industries do |t|
      t.belongs_to :merchant_profile
      t.belongs_to :industry

      t.timestamps null: false
    end
  end
end
