class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :type
      t.string :status, default: "waiting"

      t.timestamps null: false
    end
  end
end
