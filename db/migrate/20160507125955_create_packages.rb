class CreatePackages < ActiveRecord::Migration
  def change
    create_table :packages do |t|
      t.string :category
      t.integer :buzzer_hired
      t.integer :campaign_placement_month
      t.integer :campaign_placement_minute
      t.integer :price
      t.string :status
      t.references :merchant, index: true

      t.timestamps null: false
    end
  end
end
