class AddImageIdToMerchantProfile < ActiveRecord::Migration
  def change
    add_reference :images, :merchant_profile, index: true
  end
end
