class CreateVouchers < ActiveRecord::Migration
  def change
    create_table :vouchers do |t|
      t.string :title
      t.string :description
      t.date :start_date
      t.date :end_date
      t.integer :amount
      t.string :kind
      t.string :condition
      t.references :merchant, index: true
      t.references :package, index: true

      t.timestamps null: false
    end
  end
end
