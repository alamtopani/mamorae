class AddVoucherIdToImage < ActiveRecord::Migration
  def change
    add_reference :images, :voucher, index: true
  end
end
