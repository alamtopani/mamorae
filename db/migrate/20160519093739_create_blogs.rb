class CreateBlogs < ActiveRecord::Migration
  def change
    create_table :blogs do |t|
      t.string :title
      t.text :description
      t.integer :user_id
      t.string :category
      t.date :start_at
      t.date :end_at

      t.timestamps null: false
    end
  end
end
