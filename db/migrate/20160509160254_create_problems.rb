class CreateProblems < ActiveRecord::Migration
  def change
    create_table :problems do |t|
      t.string :subject
      t.string :email
      t.string :message

      t.timestamps null: false
    end
  end
end
