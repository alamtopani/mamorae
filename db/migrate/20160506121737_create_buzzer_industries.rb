class CreateBuzzerIndustries < ActiveRecord::Migration
  def change
    create_table :buzzer_industries do |t|
      t.belongs_to :buzzer_profile
      t.belongs_to :industry

      t.timestamps null: false
    end
  end
end
