class CreateMerchantProfiles < ActiveRecord::Migration
  def change
    create_table :merchant_profiles do |t|
      t.string :name
      t.string :phone
      t.string :website
      t.string :admin_name
      t.string :phone_admin
      t.string :address_1
      t.string :address_2
      t.string :country
      t.string :province_state
      t.string :city
      t.string :postal
      t.string :bio
      t.string :bank_name
      t.string :branch
      t.string :account_number
      t.references :merchant, index: true

      t.timestamps null: false
    end
  end
end
