class CreateBuzzerJobs < ActiveRecord::Migration
  def change
    create_table :buzzer_jobs do |t|
      t.belongs_to :buzzer
      t.belongs_to :job

      t.timestamps null: false
    end
  end
end
