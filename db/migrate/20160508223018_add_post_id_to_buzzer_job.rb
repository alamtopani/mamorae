class AddPostIdToBuzzerJob < ActiveRecord::Migration
  def change
    add_column :buzzer_jobs, :facebook_post_id, :string
  end
end
