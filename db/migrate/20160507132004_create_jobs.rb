class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :title
      t.string :description
      t.date :start_date
      t.date :end_date
      t.string :url
      t.string :facebook_mention
      t.string :facebook_hashtag
      t.references :merchant, index: true
      t.references :package, index: true

      t.timestamps null: false
    end
  end
end
