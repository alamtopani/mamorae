role :app, %w{mamorae@104.237.2.232}
role :web, %w{mamorae@104.237.2.232}
role :db,  %w{mamorae@104.237.2.232}

set :stage, :production

# Replace 127.0.0.1 with your server's IP address!
server '104.237.2.232', user: 'mamorae', roles: %w{web app}

set :rbenv_type,     :system
set :rbenv_ruby,     '2.2.2'
set :rbenv_prefix,   "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles,    :all

set :deploy_to,       "/home/mamorae/www"
set :rails_env,       "production"
set :branch,          "production"

set :unicorn_config_path, "/home/mamorae/www/current/config/unicorn.rb"
