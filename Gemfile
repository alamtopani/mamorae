source 'https://rubygems.org'
gem "rails", "4.2.4"
gem "sass-rails", "~> 5.0"
gem "uglifier", ">= 1.3.0"
gem "coffee-rails", "~> 4.1.0"
gem "jquery-rails"
gem "jquery-form-validator-rails"
gem "turbolinks"
gem "jbuilder", "~> 2.0"
gem "sdoc", "~> 0.4.0", group: :doc
gem "bcrypt", "~> 3.1.7"
gem "config", "~> 1.0"
gem "carrierwave"
gem "mini_magick"
gem "fog"
gem "haml-rails"
gem "bower-rails", "~> 0.10.0"
gem "bootstrap-sass", "~> 3.3", ">= 3.3.6"
gem "will_paginate",           "3.0.7"
gem "bootstrap-will_paginate", "0.0.10"
gem "mandrill-api"
gem "city-state"
gem 'omniauth'
gem 'omniauth-facebook'
gem "omniauth-google-oauth2"
gem 'devise'
gem "pg"
gem 'letter_opener'
gem 'activerecord-session_store', github: 'rails/activerecord-session_store'

group :development, :test do
  gem 'byebug'
  gem 'binding_of_caller'
  gem 'quiet_assets'
  gem 'letter_opener'
  gem 'better_errors'
  gem 'annotate'                        # Schema to model
  gem 'rspec-rails'
  gem 'factory_girl'
  gem 'shoulda-matchers', require: false
  gem 'database_cleaner'
  gem "sqlite3", "~> 1.3.7", :require => "sqlite3"
  gem 'web-console', '~> 2.0'
  gem 'spring'
end

group :production, :staging do
  gem 'unicorn'
  gem 'rails_12factor'
  gem 'capistrano', '~> 3.1.0'
  gem 'capistrano-rails', '~> 1.1.0'
  gem 'capistrano-bundler'
  gem 'capistrano-rbenv', "~> 2.0" 
  gem 'capistrano3-unicorn', "~> 0.1.1" 
  gem 'rack-cache', :require => 'rack/cache'
  gem 'capistrano-ssh-doctor', '~> 1.0'
end

gem 'nokogiri', group: [:development, :production]
gem 'rest-client', '~> 1.6.7', group: [:development, :production]